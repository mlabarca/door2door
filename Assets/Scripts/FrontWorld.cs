﻿using UnityEngine;
using System.Collections;

public class FrontWorld : MonoBehaviour {

	public Material focusMaterial;
	public Material outFocusMaterial;

	public bool isFrontPlatform;

	private BoxCollider2D groundCollider;
	private EdgeCollider2D edgeCollider;

	// Use this for initialization
	void Start () {


		groundCollider = GetComponent<BoxCollider2D>();
		edgeCollider = GetComponent<EdgeCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnSwitch(){

		//If we are in front when the switch happens change the material to out of focus
		// and disable the collider
		if (SwitchManager.Instance.inFront == true){

			//If this script is attached to a front platform, then focus out and disable collider
			// Otherwise do the opposite
			if (isFrontPlatform){
				gameObject.renderer.material = outFocusMaterial;
				groundCollider.enabled = false;
				edgeCollider.enabled = false;
			} else {
				gameObject.renderer.material = focusMaterial;
				groundCollider.enabled = true;
				edgeCollider.enabled = true;
			}
		} else {
			if (isFrontPlatform){
				gameObject.renderer.material = focusMaterial;
				groundCollider.enabled = true;
				edgeCollider.enabled = true;
			} else {
				gameObject.renderer.material = outFocusMaterial;
				groundCollider.enabled = false;
				edgeCollider.enabled = false;
			}
		}
	}


	void OnEnable(){
		GameEventManager.Switch += OnSwitch;
	}

	void OnDisable(){
		GameEventManager.Switch -= OnSwitch;
	}
}
