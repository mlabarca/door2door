﻿using UnityEngine;
using System.Collections;

public class DontDestroyThisObject : UnitySingleton<DontDestroyThisObject> {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
