﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverTextAction : MonoBehaviour {

	private Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGameOver(){
		text.enabled = true;
	}

	void OnEnable(){
		GameEventManager.GameOver += OnGameOver;
	}
	
	void OnDisable(){
		GameEventManager.GameOver -= OnGameOver;
	}


}
