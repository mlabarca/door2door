﻿using UnityEngine;
using System.Collections;

public class GameWinActions : MonoBehaviour {

	HeroControl controller;

	public string nextLevelName = "";
	public string menuLevelName = "";


	private Animator anim;
	// Use this for initialization
	void Start () {
		controller = GetComponent<HeroControl>();
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.gameWon){

			//load next level
			if (Input.GetKeyUp(KeyCode.Space)){
				Application.LoadLevel(nextLevelName);
			}

			//restart
			if (Input.GetKeyUp(KeyCode.DownArrow)){
				Application.LoadLevel(Application.loadedLevelName);
			}

			// Menu
			if (Input.GetKeyUp(KeyCode.Escape)){
				Application.LoadLevel(menuLevelName);
			}

		}

	
	}

	void OnEnable(){
		GameEventManager.GameWin += OnGameWin;
	}

	void OnDisable(){
		GameEventManager.GameWin -= OnGameWin;
	}

	void OnGameWin(){
		//disable controller, stop moving!
		controller.enabled = false;
		anim.SetBool("winning", true);
	}


}
