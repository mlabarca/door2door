﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PersistentAudioLayer : MonoBehaviour {

    [System.Serializable]
    public class NameToStatus {
        public string[] SceneNames;
        public bool[] clipStatus;
    }

    public NameToStatus clipStatus;

    private Dictionary<string, bool> clipStatusDict;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        clipStatusDict = new Dictionary<string, bool>();
        for (var index = 0; index < clipStatus.SceneNames.Length; index++) {
            clipStatusDict.Add(clipStatus.SceneNames[index], clipStatus.clipStatus[index]);

        }

        if (!audio.isPlaying) {
            audio.Play();
            OnLevelWasLoaded(0);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnLevelWasLoaded(int level) {
        if (clipStatusDict != null) {
            bool clipShouldPlay = clipStatusDict[Application.loadedLevelName];
            audio.mute = !clipShouldPlay;
        }

    }
}
