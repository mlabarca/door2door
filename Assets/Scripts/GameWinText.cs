﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameWinText : MonoBehaviour {

	private Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable(){
		GameEventManager.GameWin += OnGameWin;
	}

	void OnDisable(){
		GameEventManager.GameWin -= OnGameWin;
	}

	void OnGameWin(){

		text.enabled = true;
	}


}
