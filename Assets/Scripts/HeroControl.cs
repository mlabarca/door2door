﻿using UnityEngine;
using System.Collections;

public class HeroControl : UnitySingleton<HeroControl> {

	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.

	public float moveForce = 1f;
	public float jumpForce = 20f;
	public float maxSpeed = 5f;
	public float groundRadious = 3f;
	public LayerMask whatIsground;
	public float instructionsTime = 3f;


	private Transform groundCheck;
	private Transform groundCheck2;
	private Animator anim;
	private scrollingV2 scroll;
	public bool grounded = false;

	// Half of the colliders size, used for calculationg raycasting points
	private float halfXSize;
	private float halfYSize;
	private Renderer playerRenderer;

	private Vector3 topRaycastOrigin, topRaycastDest, midRaycastOrigin, midRaycastDest, botRaycastOrigin, botRaycastDest;  

	float h = 0;

	void Awake() 
	{
		groundCheck = transform.Find ("groundcheck");
		groundCheck2 = transform.Find ("groundcheck2");
		anim = GetComponent<Animator> ();

	} 
	// Use this for initialization
	void Start () {

		playerRenderer = GetComponent<Renderer>();

		CalculateRayCastBounds();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (grounded && Input.GetKeyDown (KeyCode.Space)) 
		{
			rigidbody2D.AddForce (Vector2.up * jumpForce);
			audio.Play();

		}
		h = Input.GetAxis ("Horizontal");

		if (h > 0 && GameManager.Instance.gameStarted == false && Time.timeSinceLevelLoad > instructionsTime){
			GameManager.Instance.StartGame();
		}
	
		}

	void FixedUpdate () {

		grounded = Physics2D.Linecast (new Vector3(groundCheck.position.x, transform.position.y, 0), groundCheck.position,
		                               whatIsground) || 
					Physics2D.Linecast (new Vector3(groundCheck2.position.x, transform.position.y, 0), groundCheck2.position,
			                           whatIsground);


		anim.SetFloat ("speed", Mathf.Abs(h));


		// Ray cast point coordinates
		topRaycastOrigin = new Vector3(transform.position.x, transform.position.y + halfYSize * 0.9f, transform.position.z);
		topRaycastDest = new Vector3(transform.position.x + halfXSize * transform.localScale.x * 1.2f, 
		                             transform.position.y + halfYSize * 0.9f, transform.position.z);

		midRaycastOrigin = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		midRaycastDest = new Vector3(transform.position.x + halfXSize * transform.localScale.x * 1.2f, 
		                             transform.position.y, transform.position.z);

		botRaycastOrigin = new Vector3(transform.position.x, transform.position.y - halfYSize, transform.position.z);
		botRaycastDest = new Vector3(transform.position.x + halfXSize * transform.localScale.x * 1.2f, 
		                             transform.position.y - halfYSize, transform.position.z);

		//Debug.Log(midRaycastDest);
		//Debug.DrawLine(botRaycastOrigin, botRaycastDest);

		// If any of these is true, we have a wall in front of us and we should not add velocity if we are not
		// in the ground
		bool blockedTop = Physics2D.Linecast(topRaycastOrigin, topRaycastDest, whatIsground);
		bool blockedMid = Physics2D.Linecast(midRaycastOrigin, midRaycastDest, whatIsground);
		bool blockedBot = Physics2D.Linecast(botRaycastOrigin, botRaycastDest, whatIsground);


		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...

		bool flyingTowardsWall = (blockedTop  || blockedMid || blockedBot);


		if(h * rigidbody2D.velocity.x < maxSpeed && !flyingTowardsWall){

			float airMultiplier = 1f;


			if (!grounded && (blockedTop  || blockedMid || blockedBot)) {
				airMultiplier = 0f;
			}
				

			// ... add a force to the player, .
			rigidbody2D.velocity = new Vector2(h * maxSpeed * airMultiplier, rigidbody2D.velocity.y);
			//rigidbody2D.AddForce(Vector2.right * h * moveForce);


		}	


		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);

		if (h > 0 && !facingRight)
						flip ();
		if (h < 0 && facingRight)
						flip ();

	}


	/// <summary>
	/// Flip Characther multiplying scale
	/// </summary>
	void flip(){
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		facingRight = !facingRight;
	}

	void OnTriggerEnter2D(Collider2D other){

//		if (other.gameObject.tag == "platform" && gameObject.rigidbody2D.velocity.y < 0){
//			anim.SetBool("ground", true);
//		}
	}


	/// <summary>
	/// Calculate distances to be used for the raycast origins and destinations
	/// </summary>
	public void CalculateRayCastBounds(){

		halfXSize = playerRenderer.bounds.size.x/2f;
		halfYSize = playerRenderer.bounds.size.y/2f;
	}


	/// <summary>
	/// Scales the ray cast bounds.
	/// </summary>
	/// <param name="scale">Scale.</param>
	public void scaleRayCastBounds(float scale){
		halfXSize *= scale;
		halfYSize *= scale;
	}


}
