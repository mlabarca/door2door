﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

	public enum doorType {
		ForegroundDoor,
		BackgroundDoor
	};


	public GameObject targetDoor;
	public LayerMask playerLayer;

	public doorType doorLocation;

	private KeyCode key;
	private bool onDoor;
	private GameObject player;

	// Use this for initialization
	void Start () {


		if (doorLocation == doorType.ForegroundDoor) {
			key = KeyCode.UpArrow;
		} else {
			key = KeyCode.DownArrow ;
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp(KeyCode.UpArrow) && onDoor && !GameManager.Instance.gameOver){
			if (audio != null){
				audio.Play();
			}
			
			SwitchFunc(player);
		}


	}

	void SwitchFunc (GameObject player) {
		player.transform.position = new Vector3(targetDoor.transform.position.x, targetDoor.transform.position.y, 
		                                        player.transform.position.z) ;
		SwitchManager.Instance.Switch();

	}


	/// <summary>
	/// These triggers manage a boolean when the player is by a door. We can't check for input inside these functions
	/// since it can lead to errors because the Input.getkey is reset in each update frame. Best to read input in an
	/// update method 
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Player"){
			onDoor = true;
			player = other.gameObject;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Player"){
			onDoor = false;
		}
	}





}
