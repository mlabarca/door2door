﻿using UnityEngine;
using System.Collections;

public class GameWinFlag : MonoBehaviour {

    private BoxCollider2D flagCollider;
    public bool isPartOfFrontWorld = true;


	// Use this for initialization
	void Start () {
        flagCollider = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnEnable() {
        GameEventManager.Switch += OnSwitch;
    }

    void OnDisable() {
        GameEventManager.Switch -= OnSwitch;
    }

	void OnTriggerEnter2D (Collider2D other){
		if (audio != null){
			audio.Play();
		}
		GameManager.Instance.GameWin();
	}

    /// <summary>
    /// Disable or enable the collider when we make the switch. IspartOfFrontWorld 
    /// Changes the behaviour depending on if this flag is the front world or the back.
    /// </summary>
    void OnSwitch() {
        if (SwitchManager.Instance.inFront) {
            flagCollider.enabled = !isPartOfFrontWorld;
        } else {
            flagCollider.enabled = isPartOfFrontWorld;
        }
    
    
    }

}
