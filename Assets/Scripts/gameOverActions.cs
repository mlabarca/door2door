﻿using UnityEngine;
using System.Collections;

public class gameOverActions : MonoBehaviour {

	public string levelSelectionLevelName = "";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.gameOver){
			if (Input.GetKeyUp(KeyCode.Space)){
				Application.LoadLevel(Application.loadedLevelName);
			}
			if (Input.GetKeyUp(KeyCode.Escape)){
				Application.LoadLevel(levelSelectionLevelName);
			}
		}
	}
}
