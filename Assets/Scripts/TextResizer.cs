﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextResizer : MonoBehaviour {

	private Text instruction;
	private RectTransform rect;


	public float referenceTextsize;
	public float referenceWidth;
	public float referenceRectWidth;
	public float referenceRectHeight;
	
	private int textSize;
	private float rectWidth;
	private float rectHeight;


	// Use this for initialization
	void Start () {
		instruction = GetComponent<Text>();
		rect = GetComponent<RectTransform>();

		textSize = Mathf.RoundToInt(Screen.width * referenceTextsize / referenceWidth);
		rectWidth = Screen.width * referenceRectWidth / referenceWidth;
		rectHeight = Screen.width * referenceRectHeight / referenceWidth;
		
		rect.sizeDelta = new Vector2(rectWidth, rectHeight);
		instruction.fontSize = textSize;

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
