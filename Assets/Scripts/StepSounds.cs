﻿using UnityEngine;
using System.Collections;
using MCB;

public class StepSounds : UnitySingleton<StepSounds> {


    public AudioClip stepSound;
    public float stepLoopDelay = 0.1f;


    private float timer;

	// Use this for initialization
	void Start () {
	    if (audio == null){
            Debug.LogError("No audio source found to play steps sounds");
        } else{
            audio.clip = stepSound;
        }
	}
	
	// Update is called once per frame
	void Update () {
        HeroControl hero = HeroControl.Instance;

        if (hero.grounded && Mathf.Abs(Input.GetAxis("Horizontal")) > 0) {
            timer += Time.deltaTime;
            if (timer > stepLoopDelay) {
                audio.Play();
                timer = 0;
            }
        }
	}

}
