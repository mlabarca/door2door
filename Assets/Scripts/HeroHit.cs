﻿using UnityEngine;
using System.Collections;
using MCB;

public class HeroHit : MonoBehaviour {

    public AudioClip shurikenHitSound;
	private Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "bullet"){
			Destroy(other.gameObject);
			anim.Play("hit");

            AudioSource gameOverSource = GameOverLimit.Instance.audio;
            AudioManager.Instance.PlayInSequence(new AudioClip[]{ shurikenHitSound, GameOverLimit.Instance.GameOverClip}, 
                                                gameOverSource);
			GameManager.Instance.EndGame();
		}


	}

}
