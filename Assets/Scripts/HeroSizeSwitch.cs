﻿using UnityEngine;
using System.Collections;

public class HeroSizeSwitch : MonoBehaviour {


	public float frontScale;
	public float backScale;


	public HeroControl controlScript;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnSwitch(){


		if (SwitchManager.Instance.inFront == true){
			Vector3 sign = gameObject.transform.localScale/ frontScale;
			gameObject.transform.localScale = new Vector3(backScale * sign.x, backScale * sign.y, backScale * sign.z);
			controlScript.scaleRayCastBounds(backScale);
		} else {
			Vector3 sign = gameObject.transform.localScale/ backScale;
			gameObject.transform.localScale = new Vector3(frontScale * sign.x, frontScale * sign.y, frontScale * sign.z);
			controlScript.scaleRayCastBounds(1f/backScale);
		}



	}

	void OnEnable(){
		GameEventManager.Switch += OnSwitch;
	}
	
	void OnDisable(){
		GameEventManager.Switch -= OnSwitch;
	}
}
