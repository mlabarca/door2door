﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonResizer : MonoBehaviour {

	public float referenceRectWidth;
	public float referenceRectHeight;
	public float referenceWidth;

	private RectTransform rect;
	private float rectWidth;
	private float rectHeight;

	// Use this for initialization
	void Start () {
	
		rect = GetComponent<RectTransform>();
		rectWidth = Screen.width * referenceRectWidth / referenceWidth ;
		rectHeight = Screen.width * referenceRectHeight / referenceWidth;
		rect.sizeDelta = new Vector2(rectWidth, rectHeight);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
