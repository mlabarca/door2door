﻿using UnityEngine;
using System.Collections;

public class cannonScript : MonoBehaviour {


	public GameObject bullet;
	public float bulletSpeed = 1f;
	public float bulletRotationSpeed = 1f;
	public float cannonFrequencyTime = 1f;
	public bool shootAtStart;

	private Animator anim;


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		if (shootAtStart){
			ShootBullet();
		}
		
		StartCoroutine(CannonShoot());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Determines whether this  CANNON instance can  shoot.
	/// </summary>
	/// <returns><c>true</c> if this instance cannon shoot; otherwise, <c>false</c>.</returns>
	private IEnumerator CannonShoot(){

		while (true){

			yield return new WaitForSeconds(cannonFrequencyTime);
			ShootBullet();

		}
	}

	/// <summary>
	/// Shoots the bullet right away
	/// </summary>
	private void ShootBullet(){
		anim.SetTrigger("fire");
		audio.Play ();                  /// linea para sonido de disparo del canion
		
		GameObject spawnedBullet = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
		if (transform.localScale.x > 0) {
			spawnedBullet.rigidbody2D.velocity = new Vector2(-bulletSpeed, 0);
		} else {
			spawnedBullet.rigidbody2D.velocity = new Vector2(bulletSpeed, 0);
		}
		
		spawnedBullet.rigidbody2D.angularVelocity = bulletRotationSpeed;
	}


}
