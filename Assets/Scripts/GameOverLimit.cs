﻿using UnityEngine;
using System.Collections;
using MCB;

public class GameOverLimit : UnitySingleton<GameOverLimit> {

    public AudioClip fallClip;
    public AudioClip GameOverClip;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other){
        AudioManager.Instance.PlayInSequence(new AudioClip[] { fallClip, GameOverClip }, audio);
		GameManager.Instance.EndGame();
	}

}
