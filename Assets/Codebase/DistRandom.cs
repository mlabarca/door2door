﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
// @Author: Mauricio Labarca 

/// <summary>
/// Creates random generator out a list of items with a non uniform 
/// distribution using cumulative probabilities
/// </summary>
public class DistRandom<T> {

	// l
	private List<KeyValuePair<T, double>> probList;
	private System.Random generator = new System.Random();

	private bool notAscending;
	private T[] originalOrder;

	/// <summary>
	/// Initializes a new instance of the <see cref="DistRandom"/> class.
	/// This is basically a random number generator that uses a custom probability 
	/// Distribution
	/// </summary>
	/// <param name="items">Items.</param>
	/// <param name="probabilities">Probabilities.</param>
	/// <param name="notAscending">If set to <c>false</c> the list will not be sorted.</param>
	public DistRandom (T[] items, double[] probabilities, bool notAscending = true){

		// Checking arguments.
		if (items.Count() != probabilities.Count()){
			throw new ArgumentException("Items collection length must match probabilities array length");
		}
		if (probabilities.Sum() != 100.0){
			throw new ArgumentException("sum of probabilities distribution array must be 100%");
		}

		// Record the first order of probabilities given
		originalOrder = items;

		probList = new List<KeyValuePair<T, double>>();

		// Populate list with pairs of string items and probabilities
		for (int i = 0; i < items.Count(); i++){
			probList.Add( new KeyValuePair<T, double>(items[i], probabilities[i]));
		}


		// Sort the list from lowest to highest 
		if (notAscending){
			probList.Sort( (x, y) => x.Value.CompareTo(y.Value));
		}
	}

	/// <summary>
	/// Returns resulting item out of selection using the given 
	/// distribution.
	/// </summary>
	public T Next(){
		double diceroll = generator.NextDouble()*100.0;
		double cumulative = 0.0;

		// 0 for numeric type, null for reference type
		T selected = default(T);
		
		for (int  i = 0; i < probList.Count ; i++){
			cumulative += probList[i].Value;
			if (diceroll < cumulative){

				selected = probList[i].Key;
				break;
			}
		}
		return selected;
	}

	/// <summary>
	/// Creates a new probability distribution. Assumes they are in the same 
	/// order as when the object was first created
	/// </summary>
	/// <param name="probabilities">Probabilities.</param>
	public void NewDistribution(double[] probabilities){
		// Dump current list of pairs
		probList.Clear();

		// Recreat each pair using the original order at object creation. 
		// This way you only have to pass one array to change probabilities
		for (int i = 0; i < originalOrder.Count(); i++){
			probList.Add(new KeyValuePair<T, double>(originalOrder[i], probabilities[i]));
		}

		// Sort the list from lowest to highest 
		probList.Sort((x, y) => x.Value.CompareTo(y.Value));

	}

}
