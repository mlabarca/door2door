﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Use this script in a quad the size of your background, 
/// With your background as a material.
/// Quad should be covered by a separate depth only camera 
/// away from your main camera, which should also be depth only.
/// </summary>
public class scrollingV2 : MonoBehaviour {




	public bool on = false;
	
	private float scrollSpeed = 0.1f;
	private float offset = 0f;

	public float speed{
		get{
			return scrollSpeed;
		}

		set {
			scrollSpeed = value;
		}
	}

	public bool toTheRight = true;

	private Material _material;
	
	void Awake () {
		
		_material = renderer.material;
		
		if (on) {
			
			TurnOn ();
			
		} else {
			
			TurnOff ();
			
		}
		
	}
	
	void Update () {
		
		float rate = scrollSpeed * Time.deltaTime;
		
		if (on) {
			if (toTheRight)
				offset += rate;
			else
				offset -= rate;
			_material.mainTextureOffset = new Vector2 (offset, 0);
		}
//		else if (!on && offset > 0.0f) {
//			offset = Mathf.Max(0.0f, offset - rate);
//			_material.mainTextureOffset = new Vector2 (offset, 0);
//		}
		
	}
	
	public void TurnOn () {
		
		on = true;

//		_material.mainTextureOffset = new Vector2 (0.0F,0f);

		
	}
	
	public void TurnOff () {
		
		on = false;
		//_material.mainTextureOffset = new Vector2 (0.5F, 0f);

		
	}

	/// <summary>
	/// Changes speed by multiplying by the provided percent.
	/// 1.0 means no change, percent > 1 is an increase. 
	/// 2.0 would be a 200% or twice the speed.
	/// </summary>
	/// <param name="percent">Percent of change, decimal notation</param>
	public void SpeedChange(float percent){
		scrollSpeed *= percent;
	}
	
}

