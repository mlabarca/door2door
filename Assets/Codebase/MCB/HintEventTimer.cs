﻿using UnityEngine;
using System.Collections;
using System;

namespace MCB {
    public class HintEventTimer : UnitySingleton<HintEventTimer> {

        // Delay between each hint when they start.
        public float hintDelay = 10f;
        
        // List of methods to execute when a hint should appear.
        // Methods with no input parameters and void return.
        public Action nextHintHandler;

        public bool offsetByInstructionLength = true;
        public AudioClip[] instructionClips;


        private bool givingHints = false;
        private float timer = 0f;
        private float audioOffset = 0f;

        // Use this for initialization
        void Start() {
            if (offsetByInstructionLength) {
                foreach (var clip in instructionClips) {
                    audioOffset += clip.length;
                }
            }
        }

        // Update is called once per frame
        void Update() {
            if (givingHints) {
                timer += Time.deltaTime;
                if ((timer >= hintDelay + audioOffset) && nextHintHandler != null) {
                    nextHintHandler();
                    ResetTimer();
                }
            }
        }

        public void StartHints() {
            givingHints = true;
            ResetTimer();
        }

        public void StopHints() {
            givingHints = false;
        }

        public void ResetTimer() {
            timer = 0f;
        }

    }
}
