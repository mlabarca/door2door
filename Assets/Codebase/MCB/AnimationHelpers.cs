﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace MCB {
    public static class AnimationHelpers {



        /// <summary>
        /// Animates the vector 3 array given, at the same time, until each of their targets is reached or max animation
        /// time passes. You must pass anonymous functions that get and set the value of each vector, a Func for the getter
        /// and an Action for the setter.
        /// Curve list, stopping criteria or animation time are optional.
        /// </summary>
        /// <returns>The transform.</returns>
        /// <param name="getters">Getters.</param>
        /// <param name="setters">Setters.</param>
        /// <param name="targetVectors">Target vectors.</param>
        /// <param name="baseAnimationSpeeds">Base animation speeds.</param>
        /// <param name="curveList">Curve list.</param>
        /// <param name="minimumDistance">Minimum distance.</param>
        /// <param name="maxAnimationTime">Max animation time.</param>
        public static IEnumerator AnimateTransform(Func<Vector3>[] getters, Action<Vector3>[] setters,
                                                 Vector3[] targetVectors, float[] baseAnimationSpeeds,
                                                 AnimationCurve[] curveList = null, float[] minimumDistance = null,
                                                 float maxAnimationTime = 10000) {

            // If we don't receive a curve in params, initialize curvelist to hold a constant velocity curve (flat line).
            if (curveList == null) {

                curveList = new AnimationCurve[targetVectors.Length];
                for (int index = 0; index < curveList.Length; index++) {
                    curveList[index] = new AnimationCurve(new Keyframe(1, 1), new Keyframe(1, 1));
                }
            }

            // Same fore minimum distance, default stopping criteria of 
            if (minimumDistance == null) {
                minimumDistance = new float[curveList.Length];
                for (int index = 0; index < curveList.Length; index++) {
                    minimumDistance[index] = 0.01f;
                }
            }

            // Calculating all total distances
            float[] totalDistances = new float[getters.Length];
            for (int i = 0; i < targetVectors.Length; i++) {

                // Measure distance for the proerty we are iterting. we have to use () since what we pass are Functions.
                totalDistances[i] = Vector3.Distance(getters[i](), targetVectors[i]);

            }


            float animationTimer = 0f;
            while (animationTimer < maxAnimationTime) {

                int skippedAnimations = 0;
                for (int index = 0; index < targetVectors.Length; index++) {
                    float currentDifference;

                    currentDifference = Vector3.Distance(getters[index](), targetVectors[index]);
                    if (currentDifference > minimumDistance[index]) {

                        // Calculate proper speed
                        float normalizedDifference = currentDifference / totalDistances[index];
                        float currentSpeed = curveList[index].Evaluate(normalizedDifference) * baseAnimationSpeeds[index];

                        // Calculate proper step 
                        float step = currentSpeed * Time.deltaTime;

                        // Set the variable to the new value using the provided setting function, e.g. transfrom.position =
                        // value inside parenthesis. 
                        setters[index](Vector3.MoveTowards(getters[index](), targetVectors[index], step));


                    } else {
                        skippedAnimations++;
                    }
                }

                // If we skipped animation for all vectors then we are done, no need to keep this couroutine alive.
                if (skippedAnimations >= getters.Length) {
                    break;
                } else {
                    animationTimer += Time.deltaTime;
                    yield return null;
                }

            }

            yield break;

        }






        /// <summary>
        /// Plays the audio clip if it exists and waits for its length.
        /// </summary>
        /// <returns>The and wait.</returns>
        /// <param name="source">Source.</param>
        /// <param name="clip">Clip.</param>
        /// <param name="extraDelay">Extra delay.</param>
        public static IEnumerator PlayAndWait(AudioSource source, AudioClip clip, float extraDelay = 0f) {

            if (clip != null) {
                source.clip = clip;
                source.Play();
                yield return new WaitForSeconds(source.clip.length + extraDelay);
            }

            yield break;
        }


        /// <summary>
        /// Plays the audio clip if it exists and waits for its length, loading it from resources
        /// </summary>
        /// <returns>The and wait.</returns>
        /// <param name="source">Source.</param>
        /// <param name="clipLocation">Clip location.</param>
        /// <param name="extraDelay">Extra delay.</param>
        public static IEnumerator PlayAndWait(AudioSource source, string clipLocation, float extraDelay = 0f) {

            AudioClip clip = Resources.Load<AudioClip>(clipLocation);

            // This is the part where we could call the other  overload with the clip parameter, but unfortunately
            // You can't call coroutines from static classes.
            if (clip != null) {
                source.clip = clip;
                source.Play();

                yield return new WaitForSeconds(source.clip.length + extraDelay);
            }

            yield break;
        }
    }
}
