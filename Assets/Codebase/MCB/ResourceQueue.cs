﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// Disable warning about events not being used.
#pragma warning disable 0067

namespace MCB {
    public class ResourceQueue<TResource>  {

        [HideInInspector]
        public TResource currentResource;

        // Will handle the list of all resources to be used, first In, first out.
        private Queue<TResource> resourceQueue;

        public event Action NextResourceHandler;
        public event Action EndResourcesHandler;


        /// <summary>
        /// Populate the queue passing an array, for example an array obtained using 
        /// Resources.Loadall().
        /// </summary>
        /// <param name="resourceArray"></param>
        public ResourceQueue(TResource[] resourceArray){
            this.resourceQueue = new Queue<TResource>(resourceArray);
            
        }

        /// <summary>
        /// To be called when next resource is needed. Will fire any methods  registered to 
        /// NextResourceHandler event every next resource, until there are no more resources, 
        /// then anything registered to the EndResourcesHandler event.
        /// </summary>
        
        public void NextResource() {
            if (resourceQueue.Count > 0) {
                currentResource = resourceQueue.Dequeue();

                OnNextResource();

            } else {
                OnEndResources();
            }
        }

        /// <summary>
        /// Fire NextResourceHandler() event only if there is something registed to it.
        /// </summary>
        private void OnNextResource() {
            if (NextResourceHandler != null) {
                NextResourceHandler();
            }
        }

        /// <summary>
        /// Fire EndResourcesHandler() event only if there is something registed to it.
        /// </summary>
        private void OnEndResources() {
            if (EndResourcesHandler != null) {
                EndResourcesHandler();
            }
        }



        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
