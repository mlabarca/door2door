﻿using UnityEngine;
using System.Collections;


namespace MCB {
    // Struct to mimic vector2, but for ints. Meant to be used for cell coordinates In the grid.
    [System.Serializable]
    public struct CellCoord {
        public int row;
        public int col;

        public CellCoord(int row, int col) {
            this.row = row;
            this.col = col;
        }


        // Everything below this point is for testing equality for this value type struct, the way
        // It is suggested in MSDN.
        public override bool Equals(System.Object obj) {
            // If parameter is null return false.
            if (obj == null) {
                return false;
            }

            // If parameter cannot be cast to CellCoord return false.
            CellCoord p = (CellCoord)obj;
            if ((System.Object)p == null) {
                return false;
            }

            // Return true if the fields match:
            return (row == p.row) && (col == p.col);
        }

        public bool Equals(CellCoord p) {
            // If parameter is null return false:
            if ((object)p == null) {
                return false;
            }

            // Return true if the fields match:
            return (row == p.row) && (col == p.col);
        }

        public override int GetHashCode() {
            return row ^ col;
        }

        public static bool operator ==(CellCoord a, CellCoord b) {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b)) {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }

            // Return true if the fields match:
            return (a.row == b.row) && (a.col == b.col);
        }

        public static bool operator !=(CellCoord a, CellCoord b) {
            return !(a == b);
        }

    }
}
