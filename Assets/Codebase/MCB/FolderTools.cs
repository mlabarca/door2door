﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace MCB {
    public static class FolderTools {


        /// <summary>
        /// Gets a list of the names of each folder located in the given path. 
        /// Paths are relative to assets folder location, so if we need a list of
        /// names with all folders inside the resources folder we would type:
        /// FolderTools.getSubfolderNames("/Resources/");
        /// </summary>
        /// <returns>The subfolder names.</returns>
        /// <param name="pathFromAssetsFolder">Path from assets folder.</param>
        static public string[] GetSubfolderNames(string pathFromAssetsFolder) {

            string resourcePath = Application.dataPath + pathFromAssetsFolder;
            string[] folderNameAndPaths = Directory.GetDirectories(resourcePath);

            string[] folderNames = new string[folderNameAndPaths.Length];

            for (int index = 0; index < folderNameAndPaths.Length; index++) {
                folderNames[index] = folderNameAndPaths[index].Substring(resourcePath.Length);
            }

            return folderNames;

        }
    }
}
