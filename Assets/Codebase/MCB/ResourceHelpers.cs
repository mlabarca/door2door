﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MCB {
    static class ResourceHelpers  {


        /// <summary>
        /// Loads an array of resources located the path provided relative to the Resources Folder,
        /// and returns a Dictionary with the resource's name as a key, and the resource itself
        /// as the value. Resource must inherit from unity Object, such as a Sprite or AudioClip.
        /// </summary>
        /// <typeparam name="TResource">Resource type that inherits from unity Object</typeparam>
        /// <param name="resourcePath">Path to folder with objects to load, relative to Resources 
        /// folder</param>
        /// <param name="resourcePrefix">Optional prefix of resource filenames, will be removed 
        /// from key strings</param>
        /// <returns>A dictionary with string keys and Tresource Values </returns>
        public static Dictionary<string, TResource> LoadDictionary<TResource>(string resourcePath, 
                                                                       string resourcePrefix = "")
        where TResource : Object
        {
            Dictionary<string, TResource> resultDict = new Dictionary<string, TResource>();

            TResource[] resourceArray = Resources.LoadAll<TResource>(resourcePath);

            for (var index = 0; index < resourceArray.Length; index++) {
                string name = resourceArray[index].name.Remove(0, resourcePrefix.Length);

                resultDict.Add(name, resourceArray[index]);
            }

            return resultDict;
        }
 
    }
}
