﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Text;

namespace MCB {
    public static class StringHelpers {

        public static string RemoveDuplicateCharsFast(string key) {
            // --- Removes duplicate chars using char arrays.
            int keyLength = key.Length;

            // Store encountered letters in this array.
            char[] table = new char[keyLength];
            int tableLength = 0;

            // Store the result in this array.
            char[] result = new char[keyLength];
            int resultLength = 0;

            // Loop through all characters
            foreach (char value in key) {
                // Scan the table to see if the letter is in it.
                bool exists = false;
                for (int i = 0; i < tableLength; i++) {
                    if (value == table[i]) {
                        exists = true;
                        break;
                    }
                }
                // If the letter is new, add to the table and the result.
                if (!exists) {
                    table[tableLength] = value;
                    tableLength++;

                    result[resultLength] = value;
                    resultLength++;
                }
            }
            // Return the string at this range.
            return new string(result, 0, resultLength);
        }

        // Shuffles given string using Linq
        public static string ShuffleStringExcludeOriginal(string stringToShuffle) {
            string shuffled;

            if (String.IsNullOrEmpty(stringToShuffle)) {
                throw new ArgumentNullException("stringToShuffle",
                                                "The stringToShuffle variable must not be null or empty");
            }

            if (stringToShuffle.Length < 2) {
                throw new ArgumentException("This method can not be used when the string to shuffle is less than two " +
                                            "characters long as it is not possible to exclude the original",
                                            "stringToShuffle");
            }

            do {
                shuffled = new string(
                                        stringToShuffle
                                            .OrderBy(character => Guid.NewGuid())
                                            .ToArray()
                                        );
            } while (shuffled == stringToShuffle);

            return shuffled;
        }


        /// <summary>
        /// Returns an array where  the indexes of each of the chars in the first word are in the
        /// second. assummes "dummyChar" will never be in the word.
        /// </summary>
        /// <param name="word"></param>
        /// <param name="scrambledWord"></param>
        /// <param name="dummyChar"></param>
        /// <returns></returns>
        public static int[] SolutionIndexes(string word, string scrambledWord, char dummyChar) {

            int[] result = new int[word.Length];

            for (var index = 0; index < word.Length; index++) {
                int letterIndex = scrambledWord.IndexOf(word[index]);
                if (letterIndex >= 0) {
                    result[index] = letterIndex;
                    StringBuilder sb = new StringBuilder(scrambledWord);
                    sb[letterIndex] = dummyChar;
                    scrambledWord = sb.ToString();
                } else {
                    Debug.Log(index);
                    Debug.Log(scrambledWord);
                    Debug.Log(word[index]);
                    Debug.LogError("Scrambled word does not contain one of the chars in the original");
                }

            }
            return result;
        }


    }
}
