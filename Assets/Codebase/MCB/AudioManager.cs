﻿using UnityEngine;
using System.Collections;

namespace MCB {
    public class AudioManager : UnitySingleton<AudioManager> {

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        /// <summary>
        /// Plays the sequence of clips defined in array
        /// </summary>
        /// <param name="source">Source.</param>
        /// <param name="clipArray">Clip array.</param>
        public void PlayInSequence(AudioClip[] clipArray, AudioSource source = null) {

            StopAllCoroutines();
            StartCoroutine(PlayInSequenceCo(clipArray, source));
        }

        // Plays sequence if not null. 
        public void PlayIfNotNull(AudioClip clip, AudioSource source = null) {

            PlayInSequence(new AudioClip[] { clip }, source);
        }


        /// <summary>
        /// Coroutine called by  other sound playing methods in this class.
        /// </summary>
        /// <returns>The in sequence co.</returns>
        /// <param name="source">Source.</param>
        /// <param name="clipArray">Clip array.</param>
        private IEnumerator PlayInSequenceCo(AudioClip[] clipArray, AudioSource source) {

            if (source == null) {
                source = audio;
            }

            for (int index = 0; index < clipArray.Length; index++) {
                if (clipArray[index] != null) {
                    source.clip = clipArray[index];
                    source.Play();

                    yield return new WaitForSeconds(source.clip.length);
                }
            }
            yield break;
        }

        public void StopCurrentSequence() {
            audio.Stop();
            StopAllCoroutines();
        }

    }
}
