﻿using UnityEngine;
using System.Collections;

namespace MCB {
    public class InitialInstructionTimer : UnitySingleton<InitialInstructionTimer> {

        public float repeatInstructionTime = 10f;
        public AudioClip initialInstructionClip;
        public bool repeatInstruction = true;

        private float currentTime;


        // Use this for initialization
        void Start() {
            if (repeatInstruction) {
                StartInstructions();
            }
        }

        // Update is called once per frame
        void Update() {


        }

        /// <summary>
        /// Keeps track of time and plays the audio for the initial instruction when timer reaches a defined time.
        /// </summary>
        /// <returns>The coroutine.</returns>
        private IEnumerator RepeatCoroutine() {
            while (repeatInstruction) {

                if (currentTime >= repeatInstructionTime) {
                    AudioManager.Instance.PlayIfNotNull(initialInstructionClip, audio);
                    ResetTimer();
                }
                yield return null;
                currentTime += Time.deltaTime;
            }

        }


        /// <summary>
        /// Starts playing the instructions. 
        /// </summary>
        /// <param name="startPlayingOne">If this is true we'll play one instruction right away and
        /// reset the timer.</param>
        public void StartInstructions(bool startPlayingOne = false) {
            repeatInstruction = true;

            if (startPlayingOne) {
                currentTime = repeatInstructionTime;
            } else {
                currentTime = 0f;
            }
            
            StartCoroutine(RepeatCoroutine());
        }

        public void ResetTimer() {
            currentTime = 0f;
        }

        public void StopInstruction() {
            repeatInstruction = false;
            audio.Stop();
        }
    }
}
