﻿using UnityEngine;
using System.Collections;

public class GameManager : UnitySingleton<GameManager> {

	public bool gameStarted = false;

	public bool gameOver = false;
	
	public bool gameWon = false;


	public void StartGame (){
		gameStarted = true;
		GameEventManager.TriggerGameStart();

	}

	public void EndGame(){
		gameOver = true;
		GameEventManager.TriggerGameOver();
	}

	public void GameWin(){
		gameWon = true;
		GameEventManager.TriggerGameWin();
	}




}
