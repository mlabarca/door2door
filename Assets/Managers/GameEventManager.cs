﻿public static class GameEventManager {

	public delegate void GameEvent();

	public static event GameEvent GameStart, GameOver, Switch, GameWin;

	public static void TriggerGameStart(){
		if(GameStart != null){
			GameStart();
		}
	}

	public static void TriggerGameOver(){
		if(GameOver != null){
			GameOver();
		}
	}

	public static void TriggerSwitch(){
		if (Switch != null){
			Switch();
		}
	}

	public static void TriggerGameWin(){
		if (GameWin != null){
			GameWin();
		}
	}


}