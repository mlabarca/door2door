﻿using UnityEngine;
using System.Collections;

public class SwitchManager : UnitySingleton<SwitchManager> {


	public bool inFront = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Switch (){
		GameEventManager.TriggerSwitch();
		SwitchManager.Instance.inFront = !SwitchManager.Instance.inFront;
	}



}
